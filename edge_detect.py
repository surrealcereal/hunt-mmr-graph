from PIL import Image
import numpy as np
import cv2

# Sensitivity for each segment of the image, determined by trial and error.
sensitivity = [110, 120, 255, 255, 255, 120]

for seg, rng in enumerate([(0, 2000), (2000, 2300), (2300, 2600), (2600, 2750), (2750, 3000), (3000, 5000)]):
    lower_bound, upper_bound = rng
    user_input = fr"segments\{lower_bound}-{upper_bound}.png"

    # Read the colored pixel coordinates.
    img = cv2.imread(user_input)
    edges = cv2.Canny(img, 0, sensitivity[seg])
    indices = np.where(edges != [0])
    coordinates = list(zip(indices[1], indices[0]))
    im = Image.new(mode="RGB", size=(214, 530))

    # Cast the data into an image file and save it.
    for x, y in coordinates:
        im.putpixel((x, y), (255, 0, 0))
    im.save(fr"edges\{lower_bound}-{upper_bound}.png")
