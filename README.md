# The Actual Hunt MMR Graph

![graph_github](https://i.imgur.com/kqEWj06.png)

### The code is documented with comments, so you can check it out for the grittiest of the technical details!

## Why this was done

I was always bothered by the fact that the Hunt MMR Graph isn't very representative of the truth, with it having
completely arbitrary scaling, changing from 2000 MMR per 214px to only 150 MMR at times. Therefore, I decided to take
matters into my own hands.

## How this was done: (with all the juicy technical details)

Before starting, do note that this is **only an approximation!** Due to the nature of this project (exterpolating data
from an image, smoothing and filling in data etc.) this will be inaccurate at places. But, unless Crytek decides to make
their match API public like League of Legends or Rainbow Six: Siege, I don't think we are going to get much better than
this.

First of all, I started by taking a screenshot of the graph from the game and cropping the unneccessary bits. These
files can be found under the directory `original-graph`.

![firstgraph](https://i.imgur.com/NQCteE8.png)

Then, I dissected the graph into its constituent parts by simple cropping, seeing as the graph does not scale  
linearly, and saved them into the directory `segments`. The image presented here depicts the 4* range, and I'll be using
it as an example throughout the course of this explanation as it has a very distinct shape, however keep in mind that
this process was repeated for each MMR bracket.

![2600-2750](https://i.imgur.com/KktCc3x.png)

As this graph is very stylized, it does not actually provide much useful information. In order to continue my analysis,
I got rid of all the visuals but the edges of the graph, which was done using `opencv`'s `cv2.Canny`
function. I then saved this data onto a new image with the help of `PIL` for further processing. The code for this step
can be found in `edge_detect.py`.

![edges](https://i.imgur.com/EYyG3dG.png)

As you can probably see, this is not perfect, with a lot of weird artifacting caused by the original graph having a
non-solid background. So, I went into Photoshop and cleaned them up. The result is as follows and can be found in
the `cleaned-up-edges` directory:

![cleaned-up-edges](https://imgur.com/1oW0TTE.png)

To actually get data from this image, I needed to have a continous graph, not two graphs representing edges. Therefore,
I again used Photoshop to fill the negative space in between and reduced the line count from two to one, which is a
1-4px thick graph. Here, as it is present in the `continuous-lines` directory:

![continuous-lines](https://i.imgur.com/Bv4IGiO.png)

This photo is finally ready for some processing! The further steps were all done inside `data.py`, so feel free to
follow along from there.

To begin, I used `opencv` paired with `numpy` to detect the positions of the colored pixels and root out any  
inconsistencies that had occured, specifically one x value having more than one corresponding y value. To resolve this
problem, I just used the uppermost y value for that x value, which, while completely arbitrary, produces at most 5px of
inaccuracy, which doesn't make a lot of difference in the grander scale.

After this point I'll not be using `PIL` to demonstrate visually, rather the graphing library `matplotlib`, more
specifically one of its modules `matplotlib.pyplot`. The resulting graphs are kept as big in resolution as feasible,
therefore you can inspect their finer details by opening them in new tabs.

As there are only a finite amount of datapoints on an image, the resulting graph isn't very smooth. So, I converted the
data from the continious line into an array of x and corresponding y values. To smooth the y values out, I used the
[Savitzky-Golay Filter](https://en.wikipedia.org/wiki/Savitzky%E2%80%93Golay_filter), through its implementation in
`scipy`: `scipy.signal.savgol_filter`. This produces the following:

![savgol](https://i.imgur.com/tAZjE98.png)

Through the repetiton of this process for every coordinate pair extracted from the image, the final product is obtained:

![graph_github](https://i.imgur.com/kqEWj06.png)

However, the result produced by the Savitzky-Golay Filter is only a dataset and no mathematical function, and therefore
cannot be used to perform further mathematical operations, e.g. being plotted on other software like GeoGebra. We can
obtain a representation in form of a function through the [Power Series](https://en.wikipedia.org/wiki/Taylor_series)
and its implementation in `numpy`, `numpy.polynomial.Polynomial`. This is the result:

![taylor](https://i.imgur.com/gZCAMFQ.png)

This is how the raw dataset, the dataset produced by the Savitzky-Golay Filter and the Power Series compare:

![comparison](https://i.imgur.com/qigqm5p.png)







