import numpy as np
from scipy.signal import savgol_filter
import cv2
import matplotlib.pyplot as plt

result = list()
for rng in [(0, 2000), (2000, 2300), (2300, 2600), (2600, 2750), (2750, 3000), (3000, 5000)]:

    # Get the human created image.
    lower_bound, upper_bound = rng
    user_input = fr"continuous-lines\{lower_bound}-{upper_bound}.png"

    # Get where the colored pixels in the image are.
    img = cv2.imread(user_input)
    indices = np.where(img != [0])[:2][::-1]  # np.where returns inverted values, so we flip them back
    coordinates = sorted(list(zip(indices[0], indices[1])))

    # Get repeated y-values for an x value. I made the arbitrary decision to use the highest y-value from the
    # conflicting ones.

    x_y_map = dict()
    for x, y in coordinates:
        try:
            x_y_map[x]
        except KeyError:
            x_y_map[x] = list()
        x_y_map[x].append(y)
    for x, y_list in x_y_map.items():
        x_y_map[x] = max(y_list)

    # The images are always 214 pixels wide, however the MMR range in this window can be drastically
    # different, so we need to scale.

    # Additionally, the further down we go in an image, the higher the y-values are, which needs to be reversed for
    # the graph.

    scaling_factor = (upper_bound - lower_bound) / 213  # 0 through 213 = 214 pixels total
    x_array = np.array([x * scaling_factor + lower_bound for x in x_y_map.keys()])
    y_array = np.array([530 - y for y in x_y_map.values()])
    result.append(list(zip(x_array, y_array)))

    # The following chunk of comments is the code for the demonstration given in the README file. As they serve no
    # purpose to the construction of the final image, they are left commented, but one can uncomment them to
    # experience the demonstration more interactively, if they so please.

    # First apply the Savitzky-Golay Filter, which smooths our y-values. This is especially helpful to prevent
    # non-smooth stepping for the MMR ranges where not a lot of data per each MMR point is present, e.g. 0-2000 or
    # 3000-5000. Note that this filter does not compute a function.

    # savgol_y = savgol_filter(y_array, 50, 3)

    # Then, optionally, construct a polynomial using power series. This constructs a mathematical function and the
    # result thereof can be used for further calculations.

    # from numpy.polynomial import Polynomial
    # poly = Polynomial.fit(x_array, savgol_y, 10)

    # This function spits the resulting polynomial in a form that is intelligble by GeoGebra by converting away from
    # scientific notation, adding signs wherever necessary and multiplying coefficients with x raised to their
    # corresponding degree.

    # def get_func(poly, lower_bound, upper_bound):
    #     result = ""
    #     coefs = poly.convert().coef
    #     for deg, i in enumerate(coefs):
    #         signed = str(i)
    #         if not str(i).startswith("-"):
    #             signed = "+" + signed
    #         result += f"{signed} x**{deg} "
    #     result = result.replace("e", "*10**").replace("x", f"(x-{lower_bound})")
    #     return "Function[" + result + f", {lower_bound}, {upper_bound}]\n"
    #
    # print(get_func(poly, lower_bound, upper_bound))

    # Here is the demonstration and comparison of the datasets in graph form.

    # plt.style.use('mplstyles/github.mplstyle')
    # _, ax = plt.subplots()
    # ax.get_yaxis().set_visible(False)
    # poly_x, poly_y = poly.linspace()
    # plt.ylim(bottom=0, top=530)
    # ax.plot(x_array, y_array, color="#3572A5", label="Raw Data")
    # ax.plot(x_array, savgol_y, color="#E74C3C", label="Savitzky-Golay Filter")
    # ax.plot(poly_x, poly_y, color="#2EA043", label="Power Series")
    # ax.legend()
    # plt.show()

# Seperate the data into x and y coordinates and smooth out function values using the Savitzky-Golay Filter.
total_dataset = sorted([tup for lst in result for tup in lst])
total_dataset_x, total_dataset_y = zip(*total_dataset)
total_dataset_y = savgol_filter(total_dataset_y, 50, 3)

# Obtain final product.
plt.style.use('mplstyles/reddit.mplstyle')
_, ax = plt.subplots()
plt.title("The Actual Hunt: Showdown MMR Graph")
ax.get_yaxis().set_visible(False)
plt.ylim(bottom=0, top=530)
ax.plot(total_dataset_x, total_dataset_y, color="#DE2B25")
plt.show()
